![](https://www.codewars.com/users/AddGaming2/badges/small)

## About me

I'm a junior software developer who currently is studying computerscience and art.
I want to bring efficient and fun compute to devs & users.

### My Projects

- :video_game: Unanounced Video Game 
- Taipan: My Programming Language
- [worldwide-lgbt-resources.com](https://worldwide-lgbt-resources.com/)
- [rstd](https://github.com/RaphaelEDiener/r-c-std): A C Library for practice
- [10 Minute Python](https://addgaming.gitlab.io/10minutepython/): A German Python introduction Book

### My Philosophy

- Screw Capitalism, Software was meant to be Free and Soft. 
- Crashes are not fun.
- Inccessibility is not fun.
- Slow software is not fun.
